<?php
/**
 * @file
 * Contains Drupal\encrypt\EncryptService.
 */
namespace Drupal\encrypt;
class EncryptService {
  
  private $encryption_keys;
  protected $demo_value;
  
  
  public function __construct() {
   // $this->encryption_keys = createKeys();
    $this->demo_value = 'Upchuk';
  }
  
  public function getEncryptedValue() {
    return encrypt('Hello World!', $this->encryption_keys['public']);
  }
  
  public function getDecryptedValue() {
    return decrypt($strEncrypted, $this->encryption_keys['private']);
  }
  
  public function getDemoValue() {
    return $this->demo_value;
  }
  
  private function createKeys() {      
    // Set the key parameters
    $config = array(
        "digest_alg" => "sha512",
        "private_key_bits" => 4096,
        "private_key_type" => OPENSSL_KEYTYPE_RSA,
    );

    // Create the private and public key
    $res = <a href="http://www.php.net/manual/en/function.openssl-pkey-new.php">openssl_pkey_new</a>($config);
    // Extract the private key from $res to $privKey
    <a href="http://www.php.net/manual/en/function.openssl-pkey-export.php">openssl_pkey_export</a>($res, $privKey);
    // Extract the public key from $res to $pubKey
    $pubKey = <a href="http://www.php.net/manual/en/function.openssl-pkey-get-details.php">openssl_pkey_get_details</a>($res);

    return array(
        'private' => $privKey,
        'public' => $pubKey["key"],
        'type' => $config,
    );
  } // end createKeys function
  
  // Encrypt data using the public key
  private function encrypt($data, $publicKey)
  {
    // Encrypt the data using the public key
    <a href="http://www.php.net/manual/en/function.openssl-public-encrypt.php">openssl_public_encrypt</a>($data, $encryptedData, $publicKey);
    // Return encrypted data
    return $encryptedData;
   } //end encrypt function
  
  // Decrypt data using the private key
  private function decrypt($data, $privateKey)
  {
    // Decrypt the data using the private key
    <a href="http://www.php.net/manual/en/function.openssl-private-decrypt.php">openssl_private_decrypt</a>($data, $decryptedData, $privateKey);
    // Return decrypted data
    return $decryptedData;
  } // end decrypt function
  
} // end EncryptService class
