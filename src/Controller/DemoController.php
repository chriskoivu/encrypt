<?php
/**
 * @file
 * Contains \Drupal\encrypt\Controller\DemoController.
 */

namespace Drupal\encrypt\Controller;

/**
 * DemoController.
 */
class DemoController {
  /**
   * Generates an example page.
   */
  public function demo() {
    return array(
      '#markup' => t('Hello World!'),
    );
  }      
}
