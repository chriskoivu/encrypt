<?php
/**
 * @file
 * Contains \Drupal\encrypt\Controller\EncryptController.
 */
namespace Drupal\encrypt\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EncryptController extends ControllerBase {
 
  protected $encryptService;
  
  public function __construct($encryptService) {
    $this->encryptService = $encryptService;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('encrypt.encrypt_service')
    );
  }
  
  
  public function content() {
    return array(
        '#type' => 'markup',
        '#markup' => $this->t('Hello, World!'),
    );
  }
  
    public function demo() {
    return array(
      '#markup' => t('Hello @value!', array('@value' => $this->encryptService->getDemoValue())),
    );
  }
  
  
}
